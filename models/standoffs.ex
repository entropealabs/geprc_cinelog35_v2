defmodule Standoffs do
  use OpenSCAD

  @head_width 4
  @bolt_width 2
  @wall_thickness 4
  @standoff_height 7

  def main() do
    outer_cylinder = cylinder(h: @standoff_height, d: @head_width + @wall_thickness, center: true)

    inner_cylinder_head =
      cylinder(h: @standoff_height, d: @head_width, center: true)
      |> translate(v: [0, 0, @standoff_height / 2])

    inner_cylinder_bolt =
      cylinder(h: @standoff_height, d: @bolt_width, center: true)
      |> translate(v: [0, 0, -(@standoff_height / 2) + 1])

    inner = union([inner_cylinder_head, inner_cylinder_bolt])

    outer_cylinder
    |> difference(inner)
    |> write("standoff.scad")
  end
end
