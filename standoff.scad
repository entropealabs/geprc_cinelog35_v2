difference(){
  cylinder(center = true, d = 8, h = 7);
  union(){
    translate(v = [0, 0, 3.5]){
      cylinder(center = true, d = 4, h = 7);
    }
    translate(v = [0, 0, -2.5]){
      cylinder(center = true, d = 2, h = 7);
    }
  }
}
